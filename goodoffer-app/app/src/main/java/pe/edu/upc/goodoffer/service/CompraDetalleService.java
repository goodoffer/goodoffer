package pe.edu.upc.goodoffer.service;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.edu.upc.goodoffer.dao.CompraDao;
import pe.edu.upc.goodoffer.dao.CompraDetalleDao;
import pe.edu.upc.goodoffer.dao.impl.CompraDaoImpl;
import pe.edu.upc.goodoffer.dao.impl.CompraDetalleDaoImpl;
import pe.edu.upc.goodoffer.model.Compra;
import pe.edu.upc.goodoffer.model.CompraDetalle;

public class CompraDetalleService {
    private static CompraDetalleService instancia;
    private CompraDetalleDao compraDao;
    private CompraDetalleService() {
    }

    public static CompraDetalleService getInstancia() {
        if (instancia == null)
            instancia = new CompraDetalleService();
        return instancia;
    }
    public void insert(CompraDetalle compra, Context context) {
        try {
            compraDao = new CompraDetalleDaoImpl(context);
            compraDao.insertar(compra);
        }catch (Exception e) {
            Log.i(null, e.toString());
        }
    }
    public List<CompraDetalle> all(CompraDetalle compra, Context context) {
        List<CompraDetalle> list = new ArrayList<>();
        try{
            compraDao = new CompraDetalleDaoImpl(context);
            list = compraDao.obtener(compra.getIdCompra());
        }catch (Exception e) {
            Log.i(null, e.toString());
        }
        return list;
    }
    public void update(CompraDetalle compra, Context context) {
        try{
            compraDao = new CompraDetalleDaoImpl(context);
            compraDao.actualizar(compra);
        }catch (Exception e) {
            Log.i(null, e.toString());
        }
    }
    public void delete(int item, Context context) {
        try{
            compraDao = new CompraDetalleDaoImpl(context);
            compraDao.eliminar(item);
        }catch (Exception e) {
            Log.i(null, e.toString());
        }
    }
}
