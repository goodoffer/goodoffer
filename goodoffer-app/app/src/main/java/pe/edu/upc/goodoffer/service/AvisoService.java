package pe.edu.upc.goodoffer.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class AvisoService {

    public void find(Response.Listener response, Response.ErrorListener responseError, Context context) {
        String url = "http://condeleron.atwebpages.com/index.php/avisos";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, response ,responseError );
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public void find(String fecha, Response.Listener response, Response.ErrorListener responseError, Context context) {
        String url = "http://condeleron.atwebpages.com/index.php/avisos/" + fecha;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, response ,responseError );
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }
}

