package pe.edu.upc.goodoffer.util;

public class Constantes {
    public static String URL = "https://shrouded-stream-28699.herokuapp.com/";
    public static String CATEGORIA_ID = "categoriaId";
    public static String PRODUCTO_NOMBRE = "productoNombre";
    public static Integer USER_ID = 1;
    public static String SESION_USUARIO_ID = "usuarioId";
    public static String SESION_USUARIO_NOMBRE = "usuarioNombre";
    public static String SESION_USUARIO_CORREO = "usuarioCorreo";
    public static String SESION_USUARIO_IMAGEN = "usuarioImagen";
    public static String SESION_ACTIVA = "sesionActiva";

}
