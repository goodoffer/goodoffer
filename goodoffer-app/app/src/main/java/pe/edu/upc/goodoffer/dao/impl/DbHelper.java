package pe.edu.upc.goodoffer.dao.impl;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;


public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DbHelper(Context context) {
        // null porque se va a usar el SQLiteCursor
        super(context, "goodoffer.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /*
        db.execSQL("DROP TABLE IF EXISTS compradetalle");
        db.execSQL("DROP TABLE IF EXISTS compra");
        onCreate(db);*/

        String sql = "CREATE TABLE IF NOT EXISTS compra (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "cantidadtotal INTEGER, " +
                "descuento DECIMAL(10,2)," +
                "estado INTEGER," +
                "fecha DATE," +
                "formapago VARCHAR(1)," +
                "impuesto DECIMAL(10,2)," +
                "total DECIMAL(10,2)," +
                "totalpago DECIMAL(10,2)," +
                "idusuario INTEGER NOT NULL" +
                ")";
        db.execSQL(sql);

       String sql2 =  "CREATE TABLE IF NOT EXISTS compradetalle (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "cantidad INTEGER," +
                "preciounit DECIMAL(10,2)," +
                "subtotal DECIMAL(10,2)," +
                "idcompra INTEGER," +
               "idproducto INTEGER NOT NULL," +
               "imagen VARCHAR(250)" +
                ")";
        db.execSQL(sql2);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS compradetalle");
        db.execSQL("DROP TABLE IF EXISTS compra");
        onCreate(db);
    }

}
