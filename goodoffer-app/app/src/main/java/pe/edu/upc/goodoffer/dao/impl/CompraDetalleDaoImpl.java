package pe.edu.upc.goodoffer.dao.impl;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.edu.upc.goodoffer.dao.CompraDetalleDao;
import pe.edu.upc.goodoffer.model.CompraDetalle;

public class CompraDetalleDaoImpl implements CompraDetalleDao {
    private DbHelper _dbHelper;

    public CompraDetalleDaoImpl(Context c) {
        _dbHelper = new DbHelper(c);
    }

    public void insertar(CompraDetalle compraDetalle) throws DAOException {
        Log.i("CompraDetalleDao: ", " iniciar insertar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();
        try {
            String[] args = new String[]{
                    compraDetalle.getCantidad().toString(),
                    compraDetalle.getPrecioUnit().toString(),
                    compraDetalle.getSubTotal().toString(),
                    compraDetalle.getIdCompra().toString(),
                    compraDetalle.getIdProducto().toString(),
                    compraDetalle.getImagen()
            };
            db.execSQL("INSERT INTO compradetalle(cantidad, preciounit, subtotal, idcompra, idproducto, imagen) VALUES(?,?,?,?,?,?)", args);
            Log.i("CompraDetalleDao: ", " se insertó el producto al carrito");
        } catch (Exception e) {
            throw new DAOException("CompraDetalleDao: Error al insertar producto: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
    public List<CompraDetalle> obtener(Integer idCompraPr) throws DAOException {
        Log.i("CompraDetalleDAO: ", " inicio obtenerProducto()");
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        List<CompraDetalle> list = new ArrayList<>();

        try {
            Cursor c = db.rawQuery("SELECT id, cantidad, preciounit, subtotal, idcompra, idproducto, imagen  from compradetalle where idcompra = '"+ idCompraPr +"'", null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    CompraDetalle modelo = new CompraDetalle();
                    int idProductoDetalle = c.getInt(c.getColumnIndexOrThrow("id"));
                    Integer cantidad = c.getInt(c.getColumnIndexOrThrow("cantidad"));
                    Double precioUnit = c.getDouble(c.getColumnIndexOrThrow("preciounit"));
                    Double subTotal = c.getDouble(c.getColumnIndexOrThrow("subtotal"));
                    Integer idCompra = c.getInt(c.getColumnIndexOrThrow("idcompra"));
                    Integer idProducto = c.getInt(c.getColumnIndexOrThrow("idproducto"));
                    String imagen = c.getString(c.getColumnIndexOrThrow(("imagen")));

                    modelo.setId(idProductoDetalle);
                    modelo.setCantidad(cantidad);
                    modelo.setPrecioUnit(precioUnit);
                    modelo.setSubTotal(subTotal);
                    modelo.setIdCompra(idCompra);
                    modelo.setIdProducto(idProducto);
                    modelo.setImagen(imagen);

                    list.add(modelo);
                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            throw new DAOException("CompraDetalleDAO: Error al obtener compradetalle: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return list;
    }
    public ArrayList<CompraDetalle> buscar(Integer idProducto, Integer idCompra) throws DAOException {
        Log.i("CompraDetalleDAO: ", " inicio buscar()");
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        ArrayList<CompraDetalle> lista = new ArrayList<>();
        try {
            Cursor c = db.rawQuery("SELECT id, cantidad, preciounit, subtotal, idcompra, idproducto, imagen " +
                    "FROM compradetalle WHERE idproducto = '"+ idProducto +"' " +
                    "AND idcompra = '" + idCompra + "'", null);

            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int id = c.getInt(c.getColumnIndexOrThrow("id"));
                    Integer cantidad = c.getInt(c.getColumnIndexOrThrow("cantidad"));
                    Double preciounit = c.getDouble(c.getColumnIndexOrThrow("preciounit"));
                    Double subtotal = c.getDouble(c.getColumnIndexOrThrow("subtotal"));
                    Float idcompra = c.getFloat(c.getColumnIndexOrThrow("idcompra"));
                    Integer idproducto = c.getInt(c.getColumnIndexOrThrow("idproducto"));
                    String imagen = c.getString(c.getColumnIndexOrThrow("imagen"));

                    CompraDetalle modelo = new CompraDetalle();

                    modelo.setId(id);
                    modelo.setCantidad(cantidad);
                    modelo.setPrecioUnit(preciounit);
                    modelo.setSubTotal(subtotal);
                    modelo.setIdCompra(idCompra);
                    modelo.setIdProducto(idproducto);
                    modelo.setImagen(imagen);

                    lista.add(modelo);
                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            throw new DAOException("CompraDetalleDAO: Error al obtener compradetalle " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return lista;
    }
    public void actualizar(CompraDetalle compraDetalle) throws DAOException {
        Log.i("CompraDetalleDAO: ", "inicio actualizar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        try {
            String[] args = new String[]{
                    compraDetalle.getCantidad().toString(),
                    compraDetalle.getSubTotal().toString(),
                    compraDetalle.getIdCompra().toString(),
                    compraDetalle.getIdProducto().toString()
            };
            db.execSQL("UPDATE compradetalle SET cantidad = ?, subtotal = ? WHERE idcompra=? and idproducto = ?", args);
        } catch (Exception e) {
            throw new DAOException("CompraDetalleDAO: Error al actualizar compradetalle: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
    public void eliminar(int id) throws DAOException {
        Log.i("CompraDetalleDAO", "inicio eliminar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        try {
            String[] args = new String[]{String.valueOf(id)};
            db.execSQL("DELETE FROM compradetalle WHERE id=?", args);
        } catch (Exception e) {
            throw new DAOException("CompraDetalleDAO: Error al eliminar compradetalle: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
}
