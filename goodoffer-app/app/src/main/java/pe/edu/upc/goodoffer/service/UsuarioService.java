package pe.edu.upc.goodoffer.service;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import pe.edu.upc.goodoffer.service.model.Usuario;
import pe.edu.upc.goodoffer.util.Constantes;

public class UsuarioService {

    private static UsuarioService instancia;

    private UsuarioService() {
    }

    public static UsuarioService getInstancia() {
        if (instancia == null)
            instancia = new UsuarioService();
        return instancia;
    }

    public void login(Usuario usuario, Response.Listener response, Response.ErrorListener responseError, Context context) throws JSONException {
        String url = Constantes.URL + "usuarios/login";
        Gson gson = new Gson();
        JsonElement jsonElement = gson.toJsonTree(usuario);
        JsonObject jsonObject = (JsonObject) jsonElement;
        JSONObject jsonObject2 = new JSONObject(jsonObject.toString());
        Log.i("======>url", url);
        Log.i("======>method", "POST");
        Log.i("======>request", jsonObject.toString());
        JsonObjectRequest jsonObjReq = new
                JsonObjectRequest(Request.Method.POST,
                url, jsonObject2, response, responseError
        );
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonObjReq);
    }
    public void save(Usuario usuario, Response.Listener response, Response.ErrorListener responseError, Context context) throws JSONException {
        String url = Constantes.URL + "usuarios/";
        Gson gson = new Gson();
        JsonElement jsonElement = gson.toJsonTree(usuario);
        JsonObject jsonObject = (JsonObject) jsonElement;
        JSONObject jsonObject2 = new JSONObject(jsonObject.toString());
        Log.i("======>url", url);
        Log.i("======>method", "POST");
        Log.i("======>request", jsonObject.toString());
        JsonObjectRequest jsonObjReq = new
                JsonObjectRequest(Request.Method.POST,
                url, jsonObject2, response, responseError
        );
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonObjReq);
    }
}
