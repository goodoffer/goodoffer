package pe.edu.upc.goodoffer.util;

import android.util.Log;

import com.android.volley.Response;

public class Util {

    public static Response.ErrorListener responseError(){
        return error -> {
            error.printStackTrace();
            Log.i("======>error", error.toString());
        };
    }
}
