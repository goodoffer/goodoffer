package pe.edu.upc.goodoffer.ui.inicio;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pe.edu.upc.goodoffer.MainActivity;
import pe.edu.upc.goodoffer.R;

import pe.edu.upc.goodoffer.adapter.CategoriaAdapter;
import pe.edu.upc.goodoffer.dto.CategoriaModel;
import pe.edu.upc.goodoffer.dto.SesionDTO;
import pe.edu.upc.goodoffer.service.CategoriaService;
import pe.edu.upc.goodoffer.service.model.Categoria;
import pe.edu.upc.goodoffer.service.model.Usuario;
import pe.edu.upc.goodoffer.util.Constantes;
import pe.edu.upc.goodoffer.util.Sesion;
import pe.edu.upc.goodoffer.util.Util;

public class InicioFragment extends Fragment {

    private InicioViewModel mViewModel;

    private RecyclerView rvCategorias;
    private EditText etProducto;

    private  Button btnRegistrarUsuario;
    private  TextView tvMensaje2;
    private  Button btnBuscarProducto ;

    private CategoriaService categoriaService = CategoriaService.getInstancia();

    public static InicioFragment newInstance() {
        return new InicioFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        mViewModel = new ViewModelProvider(this).get(InicioViewModel.class);
        View root = inflater.inflate(R.layout.fragment_inicio, container, false);

        etProducto = root.findViewById(R.id.etProducto);
        rvCategorias = root.findViewById(R.id.rvCategorias);
        cargarCategorias();

        this.btnRegistrarUsuario = root.findViewById(R.id.btnRegistrarUsuario);
        this.tvMensaje2 = root.findViewById(R.id.tvMensaje2);
        this.btnBuscarProducto = root.findViewById(R.id.btnBuscarProducto);

        btnRegistrarUsuario.setOnClickListener(v -> {
            NavHostFragment.findNavController(this).navigate(R.id.action_nav_inicio_to_nav_registrate);
        });
        btnBuscarProducto.setOnClickListener(v -> {
            if(!validateProducto())
                return;
            Bundle bundle = new Bundle();
            bundle.putString(Constantes.PRODUCTO_NOMBRE, etProducto.getText().toString().trim());
            Navigation.findNavController(v).navigate(R.id.nav_productos, bundle);
        });

        inicializar();

        return root;
    }

    private void cargarCategorias(){
        Response.Listener response = (Response.Listener<JSONArray>) r -> {
            Log.i("======>response", r.toString());
            Gson gson = new Gson();
            List<Categoria> categorias = gson.fromJson(r.toString(), new TypeToken<List<Categoria>>(){}.getType());
            final List<CategoriaModel> categoriaModels = new ArrayList<>();
            for (Categoria categoria : categorias) {
                categoriaModels.add(new CategoriaModel(categoria.getId().toString(),categoria.getDescripcion(),categoria.getImagen()));
            }
            rvCategorias.setLayoutManager(new GridLayoutManager(getContext(),2));
            //rvCategorias.setLayoutManager(new LinearLayoutManager(getContext()));
            rvCategorias.setAdapter(new CategoriaAdapter(categoriaModels));
        };
        try {
            categoriaService.find(response, Util.responseError(), getContext());
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean validateProducto(){
        String value = etProducto.getText().toString().trim();
        if(value.isEmpty()){
            etProducto.setError("Escribe un producto.");
            return false;
        }
        etProducto.setError(null);
        return true;
    }

    public void inicializar(){
        SesionDTO sesion = Sesion.obtener(requireContext());
        if(sesion.isActiva()){
            btnRegistrarUsuario.setVisibility(View.GONE);
            tvMensaje2.setVisibility(View.GONE);
        }else{
            btnRegistrarUsuario.setVisibility(View.VISIBLE);
            tvMensaje2.setVisibility(View.VISIBLE);
        }
    }


}