package pe.edu.upc.goodoffer.ui.compras;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pe.edu.upc.goodoffer.R;
import pe.edu.upc.goodoffer.adapter.CompraAdapter;
import pe.edu.upc.goodoffer.dto.SesionDTO;
import pe.edu.upc.goodoffer.model.Compra;
import pe.edu.upc.goodoffer.model.CompraDetalle;
import pe.edu.upc.goodoffer.service.CompraDetalleService;
import pe.edu.upc.goodoffer.service.CompraService;

import pe.edu.upc.goodoffer.util.Sesion;

public class ComprasFragment extends Fragment {

    private ComprasViewModel mViewModel;
    private RecyclerView rvCompras;

    private CompraService compraService = CompraService.getInstancia();
    private CompraDetalleService compraDetalleService = CompraDetalleService.getInstancia();
    List<Compra> comprasModels = new ArrayList<>();
    List<CompraDetalle> compraDetalleModels = new ArrayList<>();
    public static ComprasFragment newInstance() {
        return new ComprasFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
        }
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mViewModel = new ViewModelProvider(this).get(ComprasViewModel.class);
        View root = inflater.inflate(R.layout.fragment_compras, container, false);
        rvCompras = root.findViewById(R.id.rvCompras);
        cargarCompras(root);
        cargarDetalleCompras();
        final Button btnCheckOut = root.findViewById(R.id.btnCheckOut);
        btnCheckOut.setOnClickListener(v -> {

            Bundle bundle = new Bundle();
            Navigation.findNavController(v).navigate(R.id.nav_checkout, bundle);
        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ComprasViewModel.class);
        // TODO: Use the ViewModel
    }
    private void cargarCompras(View root){
        SesionDTO sesion = Sesion.obtener(getContext());
        //cal service compras

        Compra compra = new Compra();
        compra.setIdusuario(Integer.parseInt(sesion.getId()));
        compra.setEstado(1);
        comprasModels = compraService.all(compra, getContext());
        final TextView txtCantTotal =  root.findViewById(R.id.txtCantTotal);
        final TextView txtTotal = root.findViewById(R.id.txtTotal);
        final TextView txtDescuento = root.findViewById(R.id.txtDescuentos);
        final TextView txtImpuestos = root.findViewById(R.id.txtImpuestos);
        final  TextView txtTotalPagar = root.findViewById(R.id.txtTotalPagar);
        Log.i(null, "compras usuario: " + comprasModels.size());

        if(comprasModels.size() > 0 ) {
            txtCantTotal.setText(comprasModels.get(0).getCantidadtotal().toString());
            txtTotal.setText("S/. " + comprasModels.get(0).getTotal().toString());
            txtDescuento.setText("S/. " + comprasModels.get(0).getDescuento().toString());
            txtImpuestos.setText("S/. " + comprasModels.get(0).getImpuesto());
            txtTotalPagar.setText("S/. " + comprasModels.get(0).getTotalpago());
        }
    }

    private void cargarDetalleCompras(){
        Log.i(null,"enviar lista de productos del carrito");
        //call service detalleCompras

        CompraDetalle compraDetalle = new CompraDetalle();
        compraDetalle.setIdCompra(comprasModels.get(0).getId());
        compraDetalleModels = compraDetalleService.all(compraDetalle,getContext());
        Log.i(null,"total rows: " + compraDetalleModels.size());
        rvCompras.setLayoutManager(new GridLayoutManager(getContext(),1));
        //recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        CompraAdapter adapter = new CompraAdapter(compraDetalleModels);
        adapter.cargarFragmentCompras(this);
        rvCompras.setAdapter(adapter);
    }


}