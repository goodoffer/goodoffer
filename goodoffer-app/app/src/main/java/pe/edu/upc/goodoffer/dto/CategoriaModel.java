package pe.edu.upc.goodoffer.dto;

public class CategoriaModel {

    private String id;
    private String nombres;
    private String apellidos;
    private String foto;
    private String descripcion;

    public CategoriaModel(String descripcion, String foto) {
        this.descripcion = descripcion;
        this.foto = foto;
    }

    public CategoriaModel(String id, String descripcion, String foto) {
        this.id = id;
        this.descripcion = descripcion;
        this.foto = foto;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
