package pe.edu.upc.goodoffer.dao;

import java.util.ArrayList;

import pe.edu.upc.goodoffer.dao.impl.DAOException;
import pe.edu.upc.goodoffer.model.Compra;

public interface CompraDao {
    public void insertar(Compra compra) throws Exception;
    public Compra obtener() throws DAOException;
    public ArrayList<Compra> buscar(Integer idUsuario, Integer estado) throws DAOException;
    public void actualizar(Compra compra) throws DAOException;
    public void completar(Compra compra) throws DAOException;
    public void eliminar(int id) throws DAOException;
}
