package pe.edu.upc.goodoffer.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import pe.edu.upc.goodoffer.R;
import pe.edu.upc.goodoffer.dto.CategoriaModel;
import pe.edu.upc.goodoffer.util.Constantes;

public class CategoriaAdapter extends RecyclerView.Adapter<CategoriaAdapter.ViewHolder>{

    private List<CategoriaModel> lista;

    public CategoriaAdapter(List<CategoriaModel>dataSet) {
        lista = dataSet;
    }
    @Override
    public void onBindViewHolder(@NonNull CategoriaAdapter.ViewHolder holder, int position) {
        holder.getTvDesCat().setText(lista.get(position).getDescripcion());

        Context ctx  = holder.imgCat.getContext();
        String urlFoto = lista.get(position).getFoto();
        Glide.with(ctx)
            .load(urlFoto)
            .centerCrop()
            .placeholder(R.drawable.ic_baseline_image_24)
            .into(holder.imgCat);

        holder.itemView.setOnClickListener( v -> {
            Bundle bundle = new Bundle();
            bundle.putString(Constantes.CATEGORIA_ID, lista.get(position).getId());
            Navigation.findNavController(v).navigate(R.id.nav_productos, bundle);
        });
        /*
                String foto = lista.get(position).getFoto();
        int imageId = ctx.getResources().getIdentifier(foto, "drawable", ctx.getPackageName());
        holder.getImgCat().setImageDrawable(ContextCompat.getDrawable(ctx, imageId));
        * */
    }

    @NonNull
    @Override
    public CategoriaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fila, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvDesCat;
        private final ImageView imgCat;
        public ViewHolder(@NonNull View view) {
            super(view);
            tvDesCat = (TextView) view.findViewById(R.id.tvDesCat);
            imgCat = (ImageView) view.findViewById(R.id.imgCat);
        }

        public TextView getTvDesCat() {
            return tvDesCat;
        }

        public ImageView getImgCat() {
            return imgCat;
        }
    }
}
