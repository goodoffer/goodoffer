package pe.edu.upc.goodoffer.service;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;

import pe.edu.upc.goodoffer.util.Constantes;

public class ProductoService {

    private static ProductoService instancia;

    private ProductoService() {
    }

    public static ProductoService getInstancia() {
        if (instancia == null)
            instancia = new ProductoService();
        return instancia;
    }

    public void findByCategoriaId(String categoriaId, Response.Listener response, Response.ErrorListener responseError, Context context) throws JSONException {
        String url = Constantes.URL + "productos/findByCategoriaId/" + categoriaId;
        Log.i("======>url", url);
        Log.i("======>method", "GET");
        JsonArrayRequest jsonArrayReq = new
                JsonArrayRequest(Request.Method.GET,
                url, null, response, responseError
        );
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonArrayReq);
    }

    public void findByDescripcionContaining(String descripcion, Response.Listener response, Response.ErrorListener responseError, Context context) throws JSONException {
        String url = Constantes.URL + "productos/findByDescripcionContaining/" + descripcion;
        Log.i("======>url", url);
        Log.i("======>method", "GET");
        JsonArrayRequest jsonArrayReq = new
                JsonArrayRequest(Request.Method.GET,
                url, null, response, responseError
        );
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonArrayReq);
    }


}
