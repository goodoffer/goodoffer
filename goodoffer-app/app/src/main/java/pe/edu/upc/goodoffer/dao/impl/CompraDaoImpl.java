package pe.edu.upc.goodoffer.dao.impl;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import pe.edu.upc.goodoffer.dao.CompraDao;
import pe.edu.upc.goodoffer.model.Compra;

public class CompraDaoImpl implements CompraDao {
    private DbHelper _dbHelper;
    public CompraDaoImpl(Context c) {
        _dbHelper = new DbHelper(c);
    }
    public void insertar(Compra compra) throws DAOException {

        Log.i("CompraDao", "inicio fn insertar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();
        try {
            String[] args = new String[]{
                    compra.getCantidadtotal().toString(),
                    compra.getDescuento().toString(),
                    compra.getEstado().toString(),
                    compra.getFecha().toString(),
                    compra.getFormapago(),
                    compra.getImpuesto().toString(),
                    compra.getTotal().toString(),
                    compra.getTotalpago().toString(),
                    compra.getIdusuario().toString()

            };
            db.execSQL("INSERT INTO compra(cantidadtotal, descuento,estado,fecha,formapago,impuesto,total,totalpago,idusuario)" +
                    " VALUES(?,?,?,?,?,?,?,?,?)", args);
            Log.i("CompraDao", "Se inserto la compra");
        } catch (Exception e) {
            throw new DAOException("CompraDao: Error al insertar compra: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
    public Compra obtener() throws DAOException {
        Log.i("CompraDAO", "inicio obtener()");
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        Compra modelo = new Compra();
        try {
            Cursor c = db.rawQuery("SELECT id, cantidadtotal, descuento, estado, fecha, formapago, impuesto, total, totalpago, idusuario from compra", null);
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int id = c.getInt(c.getColumnIndexOrThrow("id"));
                    Integer cantidadtotal = c.getInt(c.getColumnIndexOrThrow("cantidadtotal"));
                    Double descuento = c.getDouble(c.getColumnIndexOrThrow("descuento"));
                    Double impuesto = c.getDouble(c.getColumnIndexOrThrow("impuesto"));
                    Double total = c.getDouble(c.getColumnIndexOrThrow("total"));
                    Double totalpago = c.getDouble(c.getColumnIndexOrThrow("totalpago"));

                    modelo.setId(id);
                    modelo.setCantidadtotal(cantidadtotal);
                    modelo.setDescuento(descuento);
                    modelo.setImpuesto(impuesto);
                    modelo.setTotal(total);
                    modelo.setTotalpago(totalpago);

                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            throw new DAOException("CompraDAO: Error al obtener compra: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return modelo;
    }

    public ArrayList<Compra> buscar(Integer idUsuario, Integer estado) throws DAOException {
        Log.i("CompraDAO", "inicio buscar()");
        SQLiteDatabase db = _dbHelper.getReadableDatabase();
        ArrayList<Compra> lista = new ArrayList<>();
        try {
            Cursor c = db.rawQuery("select id, cantidadtotal, descuento, impuesto, total, totalpago from compra where idusuario = '"+ idUsuario +"' " +
                    " and estado = " + estado+ " order by id desc;", null);

            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int id = c.getInt(c.getColumnIndexOrThrow("id"));
                    Integer cantidadtotal = c.getInt(c.getColumnIndexOrThrow("cantidadtotal"));
                    Double descuento = c.getDouble(c.getColumnIndexOrThrow("descuento"));
                    Double impuesto = c.getDouble(c.getColumnIndexOrThrow("impuesto"));
                    Double total = c.getDouble(c.getColumnIndexOrThrow("total"));
                    Double totalpago = c.getDouble(c.getColumnIndexOrThrow("totalpago"));

                    Compra modelo = new Compra();
                    modelo.setId(id);
                    modelo.setCantidadtotal(cantidadtotal);
                    modelo.setDescuento(descuento);
                    modelo.setImpuesto(impuesto);
                    modelo.setTotal(total);
                    modelo.setTotalpago(totalpago);

                    lista.add(modelo);
                } while (c.moveToNext());
            }
            c.close();
        } catch (Exception e) {
            throw new DAOException("CompraDAO: Error al obtener: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
        return lista;
    }
    public void actualizar(Compra compra) throws DAOException {
        Log.i("CompraDAO: ", "inicio actualizar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        try {
            String[] args = new String[]{
                    compra.getCantidadtotal().toString(),
                    compra.getDescuento().toString(),
                    compra.getImpuesto().toString(),
                    compra.getTotal().toString(),
                    compra.getTotalpago().toString(),
                    compra.getId().toString()
            };
            db.execSQL("UPDATE compra SET cantidadtotal = ?, descuento = ?, impuesto = ?, total = ?, totalpago = ? WHERE id=? ", args);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DAOException("CompraDAO: Error al actualizar compra: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }
    public void completar(Compra compra) throws DAOException {
        Log.i("CompraDAO: ", "inicio completar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        try {
            String[] args = new String[]{
                    compra.getEstado().toString(),
                    compra.getId().toString()
            };
            db.execSQL("UPDATE compra SET estado = ? WHERE id=? ", args);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DAOException("CompraDAO: Error al actualizar compra: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public void eliminar(int id) throws DAOException {
        Log.i("CompraDAO", "inicio eliminar()");
        SQLiteDatabase db = _dbHelper.getWritableDatabase();

        try {
            String[] args = new String[]{String.valueOf(id)};
            db.execSQL("DELETE FROM compra WHERE id=?", args);
        } catch (Exception e) {
            throw new DAOException("CompraDAO: Error al eliminar compra: " + e.getMessage());
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }


}
