package pe.edu.upc.goodoffer.service;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.edu.upc.goodoffer.dao.CompraDao;
import pe.edu.upc.goodoffer.dao.impl.CompraDaoImpl;
import pe.edu.upc.goodoffer.model.Compra;

public class CompraService {
    private static CompraService instancia;
    private CompraDao compraDao;
    private CompraService() {

    }

    public static CompraService getInstancia() {
        if (instancia == null)
            instancia = new CompraService();
        return instancia;
    }

    public void insert(Compra compra, Context context) {
        try {
            compraDao = new CompraDaoImpl(context);
            compraDao.insertar(compra);
        }catch (Exception e) {
            Log.i(null, e.toString());
        }
    }
    public List<Compra> all(Compra compra, Context context) {
        List<Compra> list = new ArrayList<>();
        try{
            compraDao = new CompraDaoImpl(context);
            list = compraDao.buscar(compra.getIdusuario(), compra.getEstado());
        }catch (Exception e) {
            Log.i(null, e.toString());
        }
        return list;
    }
    public void update(Compra compra, Context context) {
        try{
            compraDao = new CompraDaoImpl(context);
            compraDao.actualizar(compra);
        }catch (Exception e) {
            Log.i(null, e.toString());
        }
    }

}
