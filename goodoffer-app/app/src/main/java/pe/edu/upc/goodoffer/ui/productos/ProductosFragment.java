package pe.edu.upc.goodoffer.ui.productos;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.android.volley.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import pe.edu.upc.goodoffer.R;
import pe.edu.upc.goodoffer.adapter.ProductoAdapter;
import pe.edu.upc.goodoffer.dto.ProductoModel;
import pe.edu.upc.goodoffer.dto.SesionDTO;
import pe.edu.upc.goodoffer.model.Compra;
import pe.edu.upc.goodoffer.service.CategoriaService;
import pe.edu.upc.goodoffer.service.CompraService;
import pe.edu.upc.goodoffer.service.ProductoService;
import pe.edu.upc.goodoffer.service.model.Categoria;
import pe.edu.upc.goodoffer.service.model.Producto;
import pe.edu.upc.goodoffer.util.Constantes;
import pe.edu.upc.goodoffer.util.Sesion;
import pe.edu.upc.goodoffer.util.Util;

public class ProductosFragment extends Fragment {

    private ProductosViewModel mViewModel;
    View root;
    private RecyclerView rvProductos;
    private Spinner spnCategoria;

    private String categoriaId = "";
    private String categoriaId2 = "";
    private String productoNombre = "";
    private List<Categoria> categorias = new ArrayList<>();

    private ProductoService productoService = ProductoService.getInstancia();
    private CategoriaService categoriaService = CategoriaService.getInstancia();
    private CompraService compraService = CompraService.getInstancia();
    public static ProductosFragment newInstance() {
        return new ProductosFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!= null){
            if(getArguments().getString(Constantes.CATEGORIA_ID) != null){
                this.categoriaId = getArguments().getString(Constantes.CATEGORIA_ID);
            }
            if(getArguments().getString(Constantes.PRODUCTO_NOMBRE) != null){
                this.productoNombre = getArguments().getString(Constantes.PRODUCTO_NOMBRE);
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        mViewModel = new ViewModelProvider(this).get(ProductosViewModel.class);
         root = inflater.inflate(R.layout.fragment_productos, container, false);

        rvProductos = root.findViewById(R.id.rvProductos);
        spnCategoria = root.findViewById(R.id.spnCategoria);

        Log.i("======>categoriaId", categoriaId);
        Log.i("======>productoNombre", productoNombre);

        spnCategoria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if(!categoriaId.isEmpty()){
                    categoriaId = "";
                    return;
                }
                if(!productoNombre.isEmpty()){
                    productoNombre = "";
                    return;
                }

                if(position > 0){
                    final Categoria categoria = categorias.get(position - 1);
                    Log.i("===>categoria elegida",position+"  "+categoria.getDescripcion());
                    categoriaId2 = categoria.getId().toString();
                    cargarProductos();
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        if(!categoriaId.isEmpty() | !productoNombre.isEmpty() ){
            cargarProductos();
        }
        cargarSpnCategoria();

        final Button btnCarroCompras = root.findViewById(R.id.btnCarroCompras);
        btnCarroCompras.setOnClickListener(v -> {

            Bundle bundle = new Bundle();
            Navigation.findNavController(v).navigate(R.id.nav_compras, bundle);
        });

        try {
            mostrarCantidadCarrito();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return root;
    }

    private List<ProductoModel> data(){
        final List<ProductoModel> lista = new ArrayList<>();
        lista.add(new ProductoModel("Cereal Sublime 300 mg","S/.11,90", "producto1"));
        lista.add(new ProductoModel("Cereal Corn Flakes 110 g","S/.3,80", "producto2"));
        lista.add(new ProductoModel("Leche UHT Gloria Light 1 l","S/.4.90", "producto3"));
        lista.add(new ProductoModel("Harina Grano de Oro 1 Kg","S/.4.90", "producto4"));
        return lista;
    }


    private void cargarProductos(){
        Response.Listener response = (Response.Listener<JSONArray>) r -> {
            Log.i("======>response", r.toString());
            Gson gson = new Gson();
            List<Producto> productos = gson.fromJson(r.toString(), new TypeToken<List<Producto>>(){}.getType());
            final List<ProductoModel> productoModels = new ArrayList<>();
            for (Producto p : productos) {
                productoModels.add(new ProductoModel(p.getId().toString(), p.getDescripcion(),"S/." + p.getPrecio().toString(), p.getImagen()));
            }
            rvProductos.setLayoutManager(new GridLayoutManager(getContext(),2));
            //recycler.setLayoutManager(new LinearLayoutManager(getContext()));
            ProductoAdapter adapter = new ProductoAdapter(productoModels);
            adapter.cargarFragment(this);
            rvProductos.setAdapter(adapter);
        };
        try {
            if(!categoriaId.isEmpty()){
                productoService.findByCategoriaId(categoriaId, response, Util.responseError(), getContext());
            }else if(!productoNombre.isEmpty()){
                productoService.findByDescripcionContaining(productoNombre, response, Util.responseError(), getContext());
            }else if(!categoriaId2.isEmpty()){
                productoService.findByCategoriaId(categoriaId2, response, Util.responseError(), getContext());
                categoriaId2 = "";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void cargarSpnCategoria(){
        Response.Listener response = (Response.Listener<JSONArray>) r -> {
            Log.i("======>response", r.toString());
            Gson gson = new Gson();
            this.categorias = gson.fromJson(r.toString(), new TypeToken<List<Categoria>>(){}.getType());
            List<String> lista = new ArrayList<>();
            lista.add("Seleccionar");
            List<String> lista2 = categorias.stream()
                    .map(c -> c.getDescripcion())
                    .collect(Collectors.toList());
            lista.addAll(lista2);
            ArrayAdapter<String> myAdapter =
                    new ArrayAdapter<String>(getContext(),
                            android.R.layout.simple_list_item_1,
                            lista);
            myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            this.spnCategoria.setAdapter(myAdapter);
        };
        try {
            this.categoriaService.find(response, Util.responseError(), getContext());
        } catch (
                JSONException e) {
            e.printStackTrace();
        }
    }
    public void mostrarCantidadCarrito(){
        try {
            SesionDTO sesion = Sesion.obtener(getContext());
            Log.i(null,"call mostrarCantidadCarrito();");
            List<Compra> comprasModels = new ArrayList<>();
            Compra compra = new Compra();
            compra.setIdusuario(Integer.parseInt(sesion.getId()));
            compra.setEstado(1);

            comprasModels = compraService.all(compra, getContext());
            final Button btnCarroCompras = root.findViewById(R.id.btnCarroCompras);
            if(comprasModels.size() > 0 ) {
                btnCarroCompras.setText("Compras (" + comprasModels.get(0).getCantidadtotal() + ")");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}