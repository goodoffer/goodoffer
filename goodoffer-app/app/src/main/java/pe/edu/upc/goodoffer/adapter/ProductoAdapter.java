package pe.edu.upc.goodoffer.adapter;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.edu.upc.goodoffer.R;
import pe.edu.upc.goodoffer.dao.CompraDao;
import pe.edu.upc.goodoffer.dao.CompraDetalleDao;
import pe.edu.upc.goodoffer.dao.impl.CompraDaoImpl;
import pe.edu.upc.goodoffer.dao.impl.CompraDetalleDaoImpl;
import pe.edu.upc.goodoffer.dto.ProductoModel;
import pe.edu.upc.goodoffer.dto.SesionDTO;
import pe.edu.upc.goodoffer.model.Compra;
import pe.edu.upc.goodoffer.model.CompraDetalle;
import pe.edu.upc.goodoffer.ui.productos.ProductosFragment;
import pe.edu.upc.goodoffer.util.Sesion;

public class ProductoAdapter extends RecyclerView.Adapter<ProductoAdapter.ViewHolder>{

    private List<ProductoModel> lista;
    List<Compra> listCarrito  = new ArrayList<>();
    private CompraDao compraDao = null;
    private CompraDetalleDao compraDetalleDao = null;
    List<CompraDetalle> listCarritoDetalle  = new ArrayList<>();
    private ProductosFragment productContext;

    public void cargarFragment(ProductosFragment context){
        this.productContext = context;
    }
    public ProductoAdapter(List<ProductoModel>dataSet) {
        lista = dataSet;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductoAdapter.ViewHolder holder, int position) {

        Context ctx  = holder.imgPro.getContext();
        String urlFoto = lista.get(position).getFoto();
        Glide.with(ctx)
                .load(urlFoto)
                .centerCrop()
                .placeholder(R.drawable.ic_baseline_image_24)
                .into(holder.imgPro);

        holder.getTvDesPro().setText(lista.get(position).getDescripcion());
        holder.getTvPrePro().setText(lista.get(position).getPrecio());

        holder.btnComprar.setOnClickListener( v -> {
            try {
                SesionDTO sesion = Sesion.obtener(v.getContext());
                Log.i(null, "sesion"+ sesion.getId() );
                compraDao = new CompraDaoImpl(v.getContext());
                listCarrito = compraDao.buscar(Integer.parseInt(sesion.getId()), 1);
                Log.i(null, "Carrito: " + listCarrito.size());
                Compra compra = new Compra();
                if(listCarrito.size() == 0) {
                    //insert compra
                    compra.setCantidadtotal(0);
                    compra.setDescuento(0D);
                    compra.setEstado(1);
                    compra.setFecha(new Date());
                    compra.setFormapago(null);
                    compra.setImpuesto(0D);
                    compra.setTotal(0D);
                    compra.setTotalpago(0D);
                    compra.setIdusuario(Integer.parseInt(sesion.getId()));
                    compraDao.insertar(compra);
                    listCarrito = compraDao.buscar(Integer.parseInt(sesion.getId()), 1);
                }

                if(listCarrito.size() > 0 ) {
                    //search product detail
                    compraDetalleDao = new CompraDetalleDaoImpl(v.getContext());
                    listCarritoDetalle = compraDetalleDao.buscar(Integer.parseInt(lista.get(position).getId()),listCarrito.get(0).getId());
                    CompraDetalle compraDetalle = new CompraDetalle();
                    int cantidad = 0;
                    Double subtotal = 0D;
                    int lengthPrecio = lista.get(position).getPrecio().length();
                    Double precio = Double.parseDouble(lista.get(position).getPrecio().substring(3,lengthPrecio));
                    if(listCarritoDetalle.size() == 0) {
                        //insert compraDetalle
                        //cantidad, preciounit, subtotal, idcompra, idproducto, imagen
                        compraDetalle.setCantidad(1);
                        compraDetalle.setPrecioUnit(precio);
                        compraDetalle.setSubTotal(precio);
                        compraDetalle.setIdCompra(listCarrito.get(0).getId());
                        compraDetalle.setIdProducto(Integer.parseInt(lista.get(position).getId()));
                        compraDetalle.setImagen(lista.get(position).getFoto());
                        compraDetalleDao.insertar(compraDetalle);
                    }else {
                        //update CompraDetalle
                        cantidad = listCarritoDetalle.get(0).getCantidad() + 1;
                        compraDetalle.setCantidad(cantidad);
                        subtotal = cantidad * precio;
                        compraDetalle.setSubTotal(subtotal);
                        compraDetalle.setIdCompra(listCarrito.get(0).getId());
                        compraDetalle.setIdProducto(Integer.parseInt(lista.get(position).getId()));
                        compraDetalleDao.actualizar(compraDetalle);
                    }
                    listCarritoDetalle = compraDetalleDao.obtener(listCarrito.get(0).getId());
                    cantidad = 0 ;
                    subtotal = 0D;
                    for (CompraDetalle compraDetalle1 :listCarritoDetalle ) {
                        cantidad += compraDetalle1.getCantidad();
                        subtotal += (compraDetalle1.getCantidad() * compraDetalle1.getPrecioUnit());
                    }
                    Double total =  subtotal;
                    Integer cantidadTotal =  cantidad;
                    Double impuestos = total * 0.18;
                    Double totalBruto = subtotal - impuestos;
                    compra.setTotal(totalBruto);
                    compra.setCantidadtotal(cantidadTotal);
                    compra.setDescuento(0.00);
                    compra.setImpuesto(impuestos);
                    compra.setTotalpago(total);
                    compra.setId(listCarrito.get(0).getId());
                    compraDao.actualizar(compra);//here
                }

                Toast toast = new Toast(v.getContext());
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setText("Se agrego al carrito el producto, IdProducto: " + lista.get(position).getId());
                toast.show();
               // Bundle bundle = new Bundle();
               // Navigation.findNavController(v).navigate(R.id.nav_compras, bundle);
                productContext.mostrarCantidadCarrito();

            }catch (Exception e) {
                Log.i(null,e.toString());
            }

        });
    }
    /*
private Integer insertarCompraDevuelveId(Compra compra) {
    Integer idCompra = null;
        try {

        }catch (Exception e) {

            Log.i(null, e.toString());
        }
        return  idCompra;
}*/


    @NonNull
    @Override
    public ProductoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fila_producto, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imgPro;
        private final TextView tvDesPro;
        private final TextView tvPrePro;
        private final Button btnComprar;

        public ViewHolder(@NonNull View view) {
            super(view);
            imgPro = (ImageView) view.findViewById(R.id.imgPro);
            tvDesPro = (TextView) view.findViewById(R.id.tvDesPro);
            tvPrePro = (TextView) view.findViewById(R.id.tvPrePro);
            btnComprar = (Button) view.findViewById(R.id.btnComprar);
        }

        public ImageView getImgPro() {
            return imgPro;
        }

        public TextView getTvDesPro() {
            return tvDesPro;
        }

        public TextView getTvPrePro() {
            return tvPrePro;
        }
        public  Button getBtnComprar(){return btnComprar;}

    }
}
