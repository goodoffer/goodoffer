package pe.edu.upc.goodoffer.util;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.fragment.app.FragmentActivity;
import java.util.GregorianCalendar;

import pe.edu.upc.goodoffer.dto.SesionDTO;
import pe.edu.upc.goodoffer.service.model.Usuario;

public class Sesion {

    public static void cargar(Context fragmentActivity, Usuario usuario){
        SharedPreferences prefs = fragmentActivity.getSharedPreferences("PREFERENCIAS", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        String nombre = usuario.getNombres() + " " + usuario.getApellidos();
        editor.putString(Constantes.SESION_USUARIO_ID,usuario.getId().toString());
        editor.putString(Constantes.SESION_USUARIO_NOMBRE, nombre);
        editor.putString(Constantes.SESION_USUARIO_CORREO, usuario.getCorreo());
        editor.putString(Constantes.SESION_USUARIO_IMAGEN, usuario.getImagen());
        editor.putBoolean(Constantes.SESION_ACTIVA, true);
        editor.commit();
    }

    public static SesionDTO obtener(Context fragmentActivity){
        SharedPreferences prefs = fragmentActivity.getSharedPreferences("PREFERENCIAS",Context.MODE_PRIVATE);
        SesionDTO sesion = new SesionDTO();
        sesion.setId(prefs.getString(Constantes.SESION_USUARIO_ID, ""));
        sesion.setNombre(prefs.getString(Constantes.SESION_USUARIO_NOMBRE, ""));
        sesion.setCorreo(prefs.getString(Constantes.SESION_USUARIO_CORREO, ""));
        sesion.setImagen(prefs.getString(Constantes.SESION_USUARIO_IMAGEN, ""));
        sesion.setActiva(prefs.getBoolean(Constantes.SESION_ACTIVA, false));
        return sesion;
    }

    public static void eliminar(Context fragmentActivity){
        SharedPreferences prefs = fragmentActivity.getSharedPreferences("PREFERENCIAS", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constantes.SESION_USUARIO_ID, "");
        editor.putString(Constantes.SESION_USUARIO_NOMBRE, "");
        editor.putString(Constantes.SESION_USUARIO_CORREO, "");
        editor.putString(Constantes.SESION_USUARIO_IMAGEN, "");
        editor.putBoolean(Constantes.SESION_ACTIVA, false);
        editor.commit();
    }

}
