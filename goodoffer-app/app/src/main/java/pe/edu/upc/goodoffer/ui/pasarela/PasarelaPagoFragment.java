package pe.edu.upc.goodoffer.ui.pasarela;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pe.edu.upc.goodoffer.R;
import pe.edu.upc.goodoffer.dao.CompraDao;
import pe.edu.upc.goodoffer.dao.impl.CompraDaoImpl;
import pe.edu.upc.goodoffer.dto.SesionDTO;
import pe.edu.upc.goodoffer.model.Compra;
import pe.edu.upc.goodoffer.ui.compras.ComprasViewModel;
import pe.edu.upc.goodoffer.util.Sesion;

public class PasarelaPagoFragment extends Fragment {

    private PasarelaPagoViewModel mViewModel;
    private CompraDao compraDao = null;
    List<Compra> listCarrito  = new ArrayList<>();
    public static PasarelaPagoFragment newInstance() {
        return new PasarelaPagoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mViewModel = new ViewModelProvider(this).get(PasarelaPagoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_pasarela_pago, container, false);
        final Button btnPay = root.findViewById(R.id.btnPay);
        btnPay.setOnClickListener(v -> {
    try{
        SesionDTO sesion = Sesion.obtener(v.getContext());
        listCarrito = compraDao.buscar(Integer.parseInt(sesion.getId()), 1);
        compraDao = new CompraDaoImpl(v.getContext());
        Compra compra = new Compra();
        compra.setEstado(2);
        compra.setId(listCarrito.get(0).getId());
        compraDao.completar(compra); //(compra);//here
        Toast toast = new Toast(v.getContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setText("Gracias!, estamos procesando su pedido");
        toast.show();
        Bundle bundle = new Bundle();
        Navigation.findNavController(v).navigate(R.id.nav_inicio, bundle);

    }catch (Exception e) {
        Log.i(null,e.toString());
    }

        });
        return root; // inflater.inflate(R.layout.fragment_pasarela_pago, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(PasarelaPagoViewModel.class);
        // TODO: Use the ViewModel
    }

}