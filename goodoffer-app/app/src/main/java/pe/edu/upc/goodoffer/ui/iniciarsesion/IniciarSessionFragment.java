package pe.edu.upc.goodoffer.ui.iniciarsesion;

import androidx.lifecycle.ViewModelProvider;
import com.android.volley.Response;
import com.google.gson.Gson;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import pe.edu.upc.goodoffer.MainActivity;
import pe.edu.upc.goodoffer.R;
import pe.edu.upc.goodoffer.service.UsuarioService;
import pe.edu.upc.goodoffer.service.model.Usuario;
import pe.edu.upc.goodoffer.util.Sesion;
import pe.edu.upc.goodoffer.util.Util;

public class IniciarSessionFragment extends Fragment {

    private IniciarSessionViewModel mViewModel;
    private EditText etCorreo;
    private EditText etClave;
    private TextView tvMsjError;
    private UsuarioService usuarioService = UsuarioService.getInstancia();

    public static IniciarSessionFragment newInstance() {
        return new IniciarSessionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        mViewModel = new ViewModelProvider(this).get(IniciarSessionViewModel.class);
        View root = inflater.inflate(R.layout.fragment_iniciar_session, container, false);
        final Button btnRegistrate = root.findViewById(R.id.button3);
        btnRegistrate.setOnClickListener(v -> {
            NavHostFragment.findNavController(this).navigate(R.id.action_nav_iniciar_sesion_to_nav_registrate);
        });

        final Button btnIniciarSesion = root.findViewById(R.id.btnIniciarSesion);
        this.etCorreo = root.findViewById(R.id.etCorreo);
        this.etClave = root.findViewById(R.id.etClave);
        this.tvMsjError = root.findViewById(R.id.tvMsjError);
        tvMsjError.setVisibility(View.GONE);
        btnIniciarSesion.setOnClickListener(v -> {
            iniciarSesion();
        });

        return root;
    }

    private void iniciarSesion(){
        Fragment fragment = this;
        if(!validateCorreo() | !validateClave()){
            return;
        }
        Response.Listener response = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("======>response", response.toString());
                Gson gson = new Gson();
                Usuario usuario = gson.fromJson(response.toString(), Usuario.class);
                if(usuario.getCorreo() != null){

                    Sesion.cargar(getActivity(), usuario);
                    ((MainActivity)getActivity()).inicializar();

                    NavHostFragment.findNavController(fragment).navigate(R.id.action_nav_iniciar_sesion_to_nav_inicio);
                } else {
                    tvMsjError.setText("Usuario y/o contraseña incorrecto.");
                    tvMsjError.setVisibility(View.VISIBLE);
                }
            }
        };
        Usuario usuario = new Usuario();
        usuario.setCorreo(etCorreo.getText().toString());
        usuario.setClave(etClave.getText().toString());
        try {
            usuarioService.login(usuario, response, Util.responseError(), getContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean validateCorreo(){
        String value = etCorreo.getText().toString().trim();
        if(value.isEmpty()){
            etCorreo.setError("Ingrese la dirección de correo electrónico.");
            return false;
        }
        etCorreo.setError(null);
        return true;
    }

    private boolean validateClave(){
        String value = etClave.getText().toString().trim();
        if(value.isEmpty()){
            etClave.setError("Ingrese una contraseña.");
            return false;
        }
        etClave.setError(null);
        return true;
    }

}