package pe.edu.upc.goodoffer.dao.impl;

public class DAOException  extends Exception {
    public DAOException(String detailMessage) {
        super(detailMessage);
    }
}
