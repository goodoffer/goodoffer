package pe.edu.upc.goodoffer.dao;

import java.util.ArrayList;
import java.util.List;
import pe.edu.upc.goodoffer.dao.impl.DAOException;
import pe.edu.upc.goodoffer.model.CompraDetalle;

public interface CompraDetalleDao {
    public void insertar(CompraDetalle compraDetalle) throws DAOException;
    public List<CompraDetalle> obtener(Integer id) throws DAOException;
    public ArrayList<CompraDetalle> buscar(Integer idProducto, Integer idCompra) throws DAOException;
    public void actualizar(CompraDetalle compraDetalle) throws DAOException;
    public void eliminar(int id) throws DAOException;
}
