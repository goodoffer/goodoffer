package pe.edu.upc.goodoffer.ui.registrate;

import static android.app.Activity.RESULT_OK;

import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import pe.edu.upc.goodoffer.R;
import pe.edu.upc.goodoffer.service.UsuarioService;
import pe.edu.upc.goodoffer.service.model.Usuario;
import pe.edu.upc.goodoffer.util.Util;
import java.util.HashMap;
import java.util.Map;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;

public class RegistrateFragment extends Fragment {

    private RegistrateViewModel mViewModel;
    private EditText etNombres;
    private EditText etApellidos;
    private EditText etCorreo;
    private EditText etNumeroDocumento;
    private EditText etClave;

    private UsuarioService usuarioService = UsuarioService.getInstancia();

    private String tipoDocumento;

    ImageView imgPicture;
    ImageButton imgCamara;

    Bitmap bitmap;
    File photoFile;
    String nombreImagen;
    String prefix = "imagen_";
    String imagen = "";

    private static final int IMAGE_CAPTURE_CODE = 2112;

    private static final String TAG = "UPLOAD-CLOUDINARY ###";
    Map config = new HashMap();
    private Uri imagePath;

    public static RegistrateFragment newInstance() {
        return new RegistrateFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mViewModel =
                new ViewModelProvider(this).get(RegistrateViewModel.class);
        View root = inflater.inflate(R.layout.fragment_registrate, container, false);

        final Button btnRegistrarUsuario = root.findViewById(R.id.btnRegistrar);
        this.etNombres = root.findViewById(R.id.etNombres);
        this.etApellidos = root.findViewById(R.id.etApellidos);
        this.etCorreo = root.findViewById(R.id.etCorreo);
        this.etNumeroDocumento = root.findViewById(R.id.etNumeroDocumento);
        this.etClave = root.findViewById(R.id.etClave);
        final Spinner spinnerTipoDocumento = root.findViewById(R.id.spinnerTipoDocumento);
        this.imgPicture = root.findViewById(R.id.imgPreview);
        this.imgCamara = root.findViewById(R.id.imgCamara);
        try {
            this.initCongif();
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgCamara.setOnClickListener(v->{
            openCamara();
        });

        ArrayAdapter<String> myAdapter =
                new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_list_item_1,
                        getResources().getStringArray(R.array.tipos_documentos));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoDocumento.setAdapter(myAdapter);

        spinnerTipoDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String itemSeleccionado = spinnerTipoDocumento.getSelectedItem().toString();
                if(itemSeleccionado.equals("DNI")) tipoDocumento = "1";
                if(itemSeleccionado.equals("RUC")) tipoDocumento = "2";
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) { }
        });

        btnRegistrarUsuario.setOnClickListener(v -> registrar());

        return root;
    }

    private void registrar(){
        if(!validateNombres() | !validateApellidos() | !validateCorreo() | !validateNumeroDocumento()
                | !validateClave()){
            return;
        }
        uploadImage();
    }

    public void registrarUsuario() {
        Fragment fragment = this;
        Response.Listener response = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("======>response", response.toString());
                Gson gson = new Gson();
                Usuario usuario = gson.fromJson(response.toString(), Usuario.class);
                if(usuario.getCorreo() != null) {
                    Toast.makeText(requireContext(), "Usuario registrado correctamente", Toast.LENGTH_LONG).show();
                    NavHostFragment.findNavController(fragment).navigate(R.id.action_nav_registrate_to_nav_iniciar_sesion);
                } else {
                    Toast.makeText(requireContext(), "Error", Toast.LENGTH_LONG).show();
                    //tvMsjError.setText("Usuario y/o contraseña incorrecto.");
                    //tvMsjError.setVisibility(View.VISIBLE);
                }
            }
        };
        Usuario usuario = new Usuario();
        usuario.setNombres(etNombres.getText().toString());
        usuario.setApellidos(etApellidos.getText().toString());
        usuario.setCorreo(etCorreo.getText().toString());
        usuario.setTipDoc(tipoDocumento);
        usuario.setNumDoc(etNumeroDocumento.getText().toString());
        usuario.setClave(etClave.getText().toString());
        usuario.setImagen(imagen);
        usuario.setEstado(true);
        try {
            usuarioService.save(usuario, response, Util.responseError(), getContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void openCamara() {
        try {
            File storageDirectory = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            long currentMillis = System.currentTimeMillis();
            nombreImagen = prefix + currentMillis;
            photoFile = File.createTempFile(nombreImagen, ".jpg", storageDirectory);
            if(photoFile.exists()){
                Intent camaraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(camaraIntent.resolveActivity(getActivity().getPackageManager()) != null){
                    Uri imageUri = FileProvider.getUriForFile(requireContext(), getActivity().getPackageName() + ".provider", photoFile);
                    imagePath = imageUri;
                    camaraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    startActivityForResult(camaraIntent, IMAGE_CAPTURE_CODE);
                }
                else
                    Toast.makeText(requireContext(), "No se puede abrir la camara", Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == IMAGE_CAPTURE_CODE && resultCode == RESULT_OK){
            bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
            imgPicture.setImageBitmap(bitmap);
            //btnGuardar.setEnabled(true);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initCongif() {

        config.put("cloud_name", "alexjalg");
        config.put("api_key","699374611225166");
        config.put("api_secret","uorGcJsA7I5cH6qpP6b121rtnrY");
//        config.put("secure", true);
        MediaManager.init(requireContext(), config);
    }

    private void uploadImage() {
        Log.d(TAG, ": "+" button clicked");

        MediaManager.get().upload(imagePath).callback(new UploadCallback() {
            @Override
            public void onStart(String requestId) {
                Log.d(TAG, "onStart: "+"started");
            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {
                Log.d(TAG, "onStart: "+"uploading");
            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                Log.d(TAG, "onStart: "+"usuccess");
                Gson gson = new Gson();
                String json = gson.toJson(resultData);
                Log.d(TAG, json);
                imagen = (String) resultData.get("secure_url");
                Log.d(TAG, imagen);
                registrarUsuario();
            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                Log.d(TAG, "onStart: "+error);
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {
                Log.d(TAG, "onStart: "+error);
            }
        }).dispatch();
    }

    private boolean validateNombres(){
        String value = etNombres.getText().toString().trim();
        if(value.isEmpty()){
            etNombres.setError("¿Como te llamas?");
            return false;
        }
        etNombres.setError(null);
        return true;
    }
    private boolean validateApellidos(){
        String value = etApellidos.getText().toString().trim();
        if(value.isEmpty()){
            etApellidos.setError("¿Como te llamas?");
            return false;
        }
        etApellidos.setError(null);
        return true;
    }
    private boolean validateCorreo(){
        String value = etCorreo.getText().toString().trim();
        final String regex = "^[^@]+@[^@]+\\.[a-zA-Z]{2,}$";
        if(value.isEmpty()){
            etCorreo.setError("Ingrese una dirección de correo electrónico.");
            return false;
        }else if(!value.matches(regex)){
            etCorreo.setError("Ingrese una dirección de correo electrónico valido.");
            return false;
        }
        etCorreo.setError(null);
        return true;
    }
    private boolean validateNumeroDocumento(){
        String value = etNumeroDocumento.getText().toString().trim();
        if(value.isEmpty()){
            etNumeroDocumento.setError("Ingrese número de documento.");
            return false;
        }
        etNumeroDocumento.setError(null);
        return true;
    }
    private boolean validateClave(){
        String value = etClave.getText().toString().trim();
        if(value.isEmpty()){
            etClave.setError("Ingrese una contraseña.");
            return false;
        }
        etClave.setError(null);
        return true;
    }

}