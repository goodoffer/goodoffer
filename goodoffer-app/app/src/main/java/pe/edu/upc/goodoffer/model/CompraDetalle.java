package pe.edu.upc.goodoffer.model;


public class CompraDetalle {

    private Integer id;
    private Integer cantidad;
    private Double precioUnit;
    private Double subTotal;
    private Integer idCompra;
    private Integer idProducto;
    private String imagen;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecioUnit() {
        return precioUnit;
    }

    public void setPrecioUnit(Double precioUnit) {
        this.precioUnit = precioUnit;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Integer getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(Integer idCompra) {
        this.idCompra = idCompra;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public String toString() {
        return "CompraDetalle{" +
                "id=" + id +
                ", cantidad=" + cantidad +
                ", precioUnit=" + precioUnit +
                ", subTotal=" + subTotal +
                ", idCompra=" + idCompra +
                ", idProducto=" + idProducto +
                '}';
    }
}
