package pe.edu.upc.goodoffer;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;

import pe.edu.upc.goodoffer.databinding.ActivityMainBinding;
import pe.edu.upc.goodoffer.dto.SesionDTO;
import pe.edu.upc.goodoffer.service.model.Usuario;
import pe.edu.upc.goodoffer.ui.inicio.InicioFragment;
import pe.edu.upc.goodoffer.util.Sesion;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;

    private NavigationView navigationView;

    private TextView txtNombre;
    private TextView txtCorreo;
    private ImageView imgUsuario;
    private TextView logout;
    private View header;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.appBarMain.toolbar.setTitle("");
        setSupportActionBar(binding.appBarMain.toolbar);
        binding.appBarMain.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG) .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        header = navigationView.getHeaderView(0);
        txtNombre = (TextView) header.findViewById(R.id.txtNombre);
        txtCorreo = (TextView) header.findViewById(R.id.txtCorreo);
        imgUsuario = (ImageView) header.findViewById(R.id.imgUsuario);
        logout = (TextView) findViewById(R.id.logout);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_inicio, R.id.nav_productos, R.id.nav_tiendas
                , R.id.nav_registrate, R.id.nav_iniciar_sesion, R.id.nav_compras, R.id.nav_checkout)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        logout.setOnClickListener(view -> cerrarSesion());
        inicializar();
        //this.isLogin = false;
    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
*/

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void inicializar(){
        SesionDTO sesion = Sesion.obtener(this);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_iniciar_sesion).setVisible(!sesion.isActiva());
        nav_Menu.findItem(R.id.nav_registrate).setVisible(!sesion.isActiva());

        if(sesion.isActiva()){
            txtNombre.setText(sesion.getNombre());
            txtCorreo.setText(sesion.getCorreo());
            Glide.with(this)
                    .load(sesion.getImagen())
                    .centerCrop()
                    .circleCrop()
                    .placeholder(R.drawable.ic_baseline_person_80)
                    .into(imgUsuario);
            txtNombre.setVisibility(View.VISIBLE);
            txtCorreo.setVisibility(View.VISIBLE);
            logout.setVisibility(View.VISIBLE);
        }else{
            Glide.with(this)
                    .load("")
                    .centerCrop()
                    .circleCrop()
                    .placeholder(R.drawable.ic_baseline_person_80)
                    .into(imgUsuario);
            txtNombre.setVisibility(View.GONE);
            txtCorreo.setVisibility(View.GONE);
            logout.setVisibility(View.GONE);
        }

                /*
        txtNombre.setText(usuario.getNombres() + " " + usuario.getApellidos());
        txtCorreo.setText(usuario.getCorreo());
        Glide.with(this)
                .load(usuario.getImagen())
                .centerCrop()
                .circleCrop()
                .placeholder(R.drawable.ic_baseline_image_24)
                .into(imgUsuario);
                 */
    }

    private void cerrarSesion(){
        Sesion.eliminar(this);
        inicializar();

    }

}