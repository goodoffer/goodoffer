package pe.edu.upc.goodoffer.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import pe.edu.upc.goodoffer.R;
import pe.edu.upc.goodoffer.dto.ProductoModel;
import pe.edu.upc.goodoffer.model.Compra;
import pe.edu.upc.goodoffer.model.CompraDetalle;
import pe.edu.upc.goodoffer.ui.compras.ComprasFragment;
import pe.edu.upc.goodoffer.ui.productos.ProductosFragment;

public class CompraAdapter extends RecyclerView.Adapter<CompraAdapter.ViewHolder>  {

    private List<CompraDetalle> lista;
    private ComprasFragment productContext;

    public void cargarFragmentCompras(ComprasFragment context){
        this.productContext = context;
    }

    public CompraAdapter(List<CompraDetalle>dataSet) {
        lista = dataSet;
    }

    @NonNull
    @Override
    public CompraAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fila_carro_compra, viewGroup, false);
        return new CompraAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.i("alejo", "Carro detalle:" + lista.size());

        Context ctx  = holder.imgPro.getContext();
        String urlFoto = lista.get(position).getImagen();
        Glide.with(ctx)
                .load(urlFoto)
                .centerCrop()
                .placeholder(R.drawable.ic_baseline_image_24)
                .into(holder.imgPro);
/*
        holder.getTvDesPro().setText("");
        holder.getTvPrePro().setText("S/. " + lista.get(position).getPrecioUnit().toString());
        holder.getTxtCantidad().setText(lista.get(position).getCantidad());
        holder.getTxtSubTotal().setText("S/. " + lista.get(position).getSubTotal().toString());
        holder.imgMas.setOnClickListener(v -> {
            //update produto
            CompraDetalle compraDetalle = new CompraDetalle();
            update(compraDetalle);
        });

        holder.imgMenos.setOnClickListener(v -> {
            CompraDetalle compraDetalle = new CompraDetalle();
            compraDetalle.setId(Integer.parseInt(holder.getTxtCodigo().getText().toString()));
            compraDetalle.setCantidad(Integer.parseInt(holder.getTxtCantidad().getText().toString()));
            update(compraDetalle);
        });

        holder.btnEliminar.setOnClickListener(v -> {
            try {

            }catch (Exception e) {
                Log.i(null, e.toString());
            }
        });
 */
    }
    private void update(CompraDetalle compraDetalle){
        try {

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtCodigo;
        private final ImageView imgPro;
        private final TextView tvDesPro;
        private final TextView tvPrePro;
        private final ImageView imgMas;
        private final ImageView imgMenos;
        private final EditText txtCantidad;
        private final TextView txtSubTotal;
        private final Button btnEliminar;

        public ViewHolder(@NonNull View view) {
            super(view);
            txtCodigo = (TextView) view.findViewById(R.id.txtCodigo);
            imgPro = (ImageView) view.findViewById(R.id.imgPro);
            tvDesPro = (TextView) view.findViewById(R.id.tvDesPro);
            tvPrePro = (TextView) view.findViewById(R.id.tvPrePro);
            imgMas = (ImageView) view.findViewById(R.id.imgMenos);
            imgMenos = (ImageView) view.findViewById(R.id.imgMenos);
            txtCantidad = (EditText) view.findViewById(R.id.txtCantidad);
            txtSubTotal = (TextView) view.findViewById(R.id.txtSubTotal);
            btnEliminar = (Button) view.findViewById(R.id.btnEliminar);
        }
        public TextView getTxtCodigo() {
            return txtCodigo;
        }

        public ImageView getImgPro() {
            return imgPro;
        }

        public TextView getTvDesPro() {
            return tvDesPro;
        }

        public TextView getTvPrePro() {
            return tvPrePro;
        }

        public ImageView getImgMas() {
            return imgMas;
        }

        public ImageView getImgMenos() {
            return imgMenos;
        }

        public EditText getTxtCantidad() {
            return txtCantidad;
        }

        public TextView getTxtSubTotal() {
            return txtSubTotal;
        }

        public Button getBtnEliminar() {
            return btnEliminar;
        }

    }

}
