package pe.edu.upc.goodoffer.dto;

public class ProductoModel {

    private String id;
    private String foto;
    private String descripcion;
    private String precio;

    public ProductoModel( String id, String descripcion, String precio, String foto) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
        this.foto = foto;
    }

    public ProductoModel( String descripcion, String precio, String foto) {
        this.descripcion = descripcion;
        this.precio = precio;
        this.foto = foto;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
