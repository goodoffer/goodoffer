package pe.edu.upc.goodoffer.model;

import java.util.Date;

public class Compra {

    private Integer id;
    private Integer cantidadtotal;
    private Double descuento;
    private Integer estado;
    private Date fecha;
    private String formapago;
    private Double impuesto;
    private Double total;
    private Double totalpago;
    private Integer idusuario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCantidadtotal() {
        return cantidadtotal;
    }

    public void setCantidadtotal(Integer cantidadtotal) {
        this.cantidadtotal = cantidadtotal;
    }

    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getFormapago() {
        return formapago;
    }

    public void setFormapago(String formapago) {
        this.formapago = formapago;
    }

    public Double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(Double impuesto) {
        this.impuesto = impuesto;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getTotalpago() {
        return totalpago;
    }

    public void setTotalpago(Double totalpago) {
        this.totalpago = totalpago;
    }

    public Integer getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Integer idusuario) {
        this.idusuario = idusuario;
    }

    @Override
    public String toString() {
        return "Compra{" +
                "id=" + id +
                ", cantidadtotal=" + cantidadtotal +
                ", descuento=" + descuento +
                ", estado=" + estado +
                ", fecha=" + fecha +
                ", formapago='" + formapago + '\'' +
                ", impuesto=" + impuesto +
                ", total=" + total +
                ", totalpago=" + totalpago +
                ", idusuario=" + idusuario +
                '}';
    }
}
