package pe.edu.upc.goodoffer.service;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import pe.edu.upc.goodoffer.util.Constantes;

public class CategoriaService {

    private static CategoriaService instancia;

    private CategoriaService() {
    }

    public static CategoriaService getInstancia() {
        if (instancia == null)
            instancia = new CategoriaService();
        return instancia;
    }

    public void find(Response.Listener response, Response.ErrorListener responseError, Context context) throws JSONException {
        String url = Constantes.URL + "categorias";
        Log.i("======>url", url);
        Log.i("======>method", "GET");
        JsonArrayRequest jsonArrayReq = new
                JsonArrayRequest(Request.Method.GET,
                url, null, response, responseError
        );
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonArrayReq);
    }

}
